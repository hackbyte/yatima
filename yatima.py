#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
$ python3.6 yatima.py

This is Cryptotalk Butler v2.

But yatima can be so much more!

May all your routines work perfectly my holy yatima!

(20180826T192514 UTC, hackbyte - daniel mitzlaff / me@hackbyte.de)

requirements: telepot, feedparser (+sgmmlib!), crypocompy, psycopg2

Stuff to know:

how to get psycopg2 for python3.6
PYTHONPATH=~/.local/lib64/python3.6/site-packages python3.6 setup.py install --prefix=~/.local

See README and CHANGELOG please.

"""

################################################################################
#
# first imports
#
import os, sys, inspect 

################################################################################
# save scriptpath
scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# save our scriptname (basename of the full blown path like above)
scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))
# (if) we want to include stuff from an include/ dir _in the path where the script lies in_!
if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
	# save the dirname of the parent directory
	scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
# or else
else:
	# save our dir
	scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# set includepath als include within scriptpath dir
includepath	= scriptpath + '/include'
# check if includepath exists
if os.path.isdir(includepath):
	# if so, set it as additional import path
	sys.path.insert(0, includepath)
# if not
else:
	# complain!!!
	print("No include path found!")
	# and gtfo
	sys.exit(23)

# pudb debugging......
from pudb import set_trace 
import logging, logging.handlers, shutil, time, datetime, asyncio
import operator, argparse, locale, json, psycopg2
################################################################################
# Now We're sure to have any needed include path.... for reasons
##import stuff_helper 
##from stuff_helper import myfunc

################################################################################
# now, parse command line arguments...
#
# create parser object and give it a description.
parser = argparse.ArgumentParser(description=scriptname + ' commandline parameters.')
# add argument -c/--config for a configfile, default will be (our_path)/config/config.json
parser.add_argument(
	"-c", "--config",
	dest="config_file",
	action="store",
	default=scriptpath+"/config/config.json",
	metavar="FILE",
	help="Use configuration from FILE")
# add argument -d/--debug as simple boolean toggle, defaults to false.
parser.add_argument(
	"-d", "--debug",
	dest="debug",
	action="store_true",
	default=False,
	help="show heavy masses(!!) of debug output.")
# add argument -t/--test for test-run, defaults to false.
parser.add_argument(
	"-t", "--test",
	dest="test",
	action="store_true",
	default=False,
	help="for testing environment")
# add an argument for some parameter (telegram api token in my case;)
parser.add_argument(
	"-a", "--api",
	dest="token",
	action="store",
	default="",
	metavar="TELEGRAM_TOKEN",
	help="Telegram bot api token to use")
# And add an argument for a owner id for telegram too
parser.add_argument(
	"-o", "--owner",
	dest="owner",
	action="store",
	default="",
	metavar="UID",
	help="Owner ID")
# mkay now the f.. parse that!! ;)
cmdline = parser.parse_args()


################################################################################
# set up and configure python logging.
#
logfilenamepattern = scriptname
# get rid of .py extension
if '.py' in logfilenamepattern:
	logfilenamepattern = logfilenamepattern.replace('.py','')

# create log object
log = logging.getLogger()
logging.getLogger(__name__).addHandler(logging.NullHandler())
# by default, log all INFO messages. ;)
log.setLevel(logging.INFO)
# Formatting for anything not syslog (so we need our own timestamp)
logformatter_main = logging.Formatter(
	'%(asctime)s.%(msecs)03d ' +
	'%(filename)-25s ' +
	'%(module)-23s ' +
	'%(funcName)-23s ' +
	'%(lineno)4d ' +
	'%(levelname)-8s ' +
	'%(message)s',
	'%Y%m%d%H%M%S')
#	'%(processName)-12s ' +
#	'%(threadName)-12s ' +

# for syslog (so we do not need our own timestamp)
logformatter_syslog = logging.Formatter(
	'%(filename)-25s ' +
	'%(module)-23s ' +
	'%(funcName)-23s ' +
	'%(lineno)4d ' +
	'%(levelname)-8s ' +
	'%(message)s')
#	'%(processName)-12s ' +
#	'%(threadName)-12s ' +

# create console log handler (stdout/stderr)
logcons = logging.StreamHandler()
# log even debug stuff to console (if enabled below;))
logcons.setLevel(logging.DEBUG)
# make it fancy
logcons.setFormatter(logformatter_main)
# and add handler to log..
log.addHandler(logcons)

# create file handler which logs debug messages too
#logfile = logging.FileHandler(config['home'] + '/allmessages.log')
#logfile.setLevel(logging.DEBUG)
#logfile.setFormatter(logformatter_main)
#log.addHandler(logfile)

# create a syslog handler
#logsys = logging.handlers.SysLogHandler(address = '/dev/log')
#logsys.setLevel(logging.INFO)
#logsys.setFormatter(logformatter_syslog)
#log.addHandler(logsys)

# create file handler which logs debug messages too
if cmdline.test:
	logfile2 = logging.FileHandler(scriptpath + '/log/' + logfilenamepattern + '_allmessages.log')
	logfile2.setLevel(logging.DEBUG)
	logfile2.setFormatter(logformatter_main)
	log.addHandler(logfile2)

################################################################################
# if debug parameter is set from commandline, make it so........
if cmdline.debug:
    log.setLevel(logging.DEBUG)

################################################################################
# we're alive!

log.debug("------------------------------------------")
log.debug("------------------------------------------")
log.debug("------------------------------------------")
log.debug("START!...")
log.debug("python version " + str(sys.version).replace('\n', ''))

################################################################################
# log what we got from commandline.....
log.debug("cmdline.config_file = "	+ str(cmdline.config_file))
log.debug("cmdline.debug = "		+ str(cmdline.debug))
log.debug("cmdline.test = "			+ str(cmdline.test))
log.debug("cmdline.token = "		+ str(cmdline.token))
log.debug("cmdline.owner = "		+ str(cmdline.owner))

################################################################################
#
# yeah..........................................................................
#

################################################################################
#
# umlautfuckup... (german umlauts fuckup maximalus)
#
import uml_helper as uml

################################################################################
#
# init config stuff...
#
log.debug("initializing config from " + cmdline.config_file)

import config_helper
# create config object which loads it's data automatically
if cmdline.config_file.strip()[:1] == "/":
	log.debug("getting config with cmdline.config_file = " + cmdline.config_file)
	config = config_helper.Yatimaconfig(cmdline.config_file)
else:
	log.debug("getting config with scriptpath + '/' + cmdline.config_file = " + scriptpath + '/' + cmdline.config_file)
	config = config_helper.Yatimaconfig(scriptpath + '/' + cmdline.config_file)

config.logfilenamepattern = logfilenamepattern

# save debug mode if set from cmdline
config.debugmode = cmdline.debug
# save testenv if set from cmdline
config.testenv = cmdline.test

################################################################################
# Because we have our defaultconfig already loaded, now we may overwrite
# some values from the cmdline....
#
# check for token and owner uid in commandline...
#
if len(str(cmdline.token)) > 0:
	config.telegram_token = cmdline.token
#
if len(str(cmdline.owner)) > 0:
	config.owner = cmdline.owner

################################################################################
#
# create file handler which logs debug messages too
#
if cmdline.debug:
	logfile = logging.FileHandler(config.log_dir + '/' + logfilenamepattern + '_' + '{:%Y_%m_%d-%H_%M_%S_%f}'.format(datetime.datetime.now()) + '.log')
	logfile.setLevel(logging.DEBUG)
	logfile.setFormatter(logformatter_main)
	log.addHandler(logfile)


################################################################################
#
# check and initialize database
#
#from db_helper import get_db_connection
#dbconn = get_db_connection(bot.config)


################################################################################
################################################################################
################################################################################
#
# create bot object with token and owner-id as parameters
#
#
# create loop object
#
log.debug("creating asyncio event loop...")
loop = asyncio.get_event_loop()

#
# create bot from our own class. ;)
#
# iport main yatima program/class
from yatima_main import Yatima
from yatima_main import EventHandler
#
# telepot! our primary resource! ;)
import telepot
import telepot.aio
import telepot.aio.helper
from telepot import message_identifier, glance, flance
from telepot.aio.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from telepot.aio.delegate import (
	include_callback_query_chat_id,
	per_message,
	per_chat_id,
	per_chat_id_in,
	per_application,
	call,
	create_open,
	pave_event_space)


# creating bot object, giving it his config and our loop (for reasons i don't
# really know/understand right now)
log.debug("getting bot object...")
bot = Yatima(config,loop)

################################################################################
#
# create task for the loop object
#
log.debug("executing loop.create_task(MessageLoop(bot).run_forever())...")
loop.create_task(MessageLoop(bot).run_forever())

################################################################################
#
# and start our endless loop. ;)
#
log.info('Yatima is fully loaded and listening as @'+bot.username+' (UID '+str(bot.id)+'), entering eternal loop...')
loop.run_forever()
log.debug("loop cancelled!")

################################################################################
################################################################################
log.debug("END!...")
