#!/bin/bash

# we need at least python 3.6!

OWNERID="999999999"

export yatima="python3.6 yatima.py -d -t -c config/config.json -o ${OWNERID}"

echo "Starting $yatima..."

$yatima
