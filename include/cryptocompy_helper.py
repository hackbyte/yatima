#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
################################################################################
################################################################################
# first imports
import os, sys, shutil, time, datetime, locale, asyncio, json
import logging, logging.handlers, inspect, operator, argparse
import psycopg2
################################################################################
################################################################################
scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))
if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
	scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
else:
	scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includepath	= scriptpath + '/include'
if os.path.isdir(includepath):
	sys.path.insert(0, includepath)
else:
	print("No include path found!")
	sys.exit(23)
################################################################################
################################################################################
from pudb import set_trace 

from telepot import glance
# cryptocompare API stuff
from cryptocompy import coin
from cryptocompy import price
################################################################################
################################################################################
log = logging.getLogger(__name__)
log.debug("START!...")
known_cryptos=coin.get_coin_list(coins='all')
################################################################################
################################################################################
#
# kurs kommando
#
def kurs_cmd(yatima,msg):
	log.debug("kurs kommando aufgerufen!")
	# grab some metadata...
	content_type, chat_type, chat_id, chat_timestamp, chat_msgid = glance(msg,long=True)
	txt=""
	if 'text' in msg:
		txt = txt + msg['text']
	if 'caption' in msg:
		# embrace it ;)
		txt = txt + msg['caption']
	rmsg = ""
	log.debug("kurs txt:".rjust(yatima.config.spacing) +' '+ txt)

	if '@' in txt:
		log.debug("@ in cmd found!")
		if not str(yatima.username).lower() in txt.lower():
			log.debug("We are " + str(yatima.username).lower())
			log.debug("Command is for " + str(txt.strip()[6:]))
			log.debug("The command is not for us, ignoring...")
			return

	# wenn /kurs mit irgendwelchen weiteren zeichen aufgerufen wurde
	if len(txt.strip()[6:]) > 0:
		coin = str(txt.strip()[6:])


	# wenn /kurs oder /kurs@BOTUSERNAME ohne parameter aufgerufen wurde
	log.debug("my username:".rjust(yatima.config.spacing) +' '+ str(yatima.username))
	if '/kurs@'+str(yatima.username).lower() in str(txt).lower():
		coin="default"
	if '/kurs' == str(txt).lower():
		coin='default'
	

	# mond easteregg
	if coin == "mond":
		rmsg = "`Doge` to the *M000000000000000N*"
		return(rmsg)
	# MILC!
	elif coin == 'milc':
		rmsg = ""
		rmsg = rmsg + str('`') + "MILC".rjust(6) + str('`/`') + "EURO".ljust(6) + str('` *') + str("4.00000000") + str('*\n\n')
		rmsg = rmsg + "*MILC*?? *M" + uuml + "n or bust*!!\n"
		rmsg = rmsg + "Source [www.milc.global](https://www.milc.global/)"
		bot.sendMessage(chat_id, rmsg, parse_mode="Markdown", disable_web_page_preview=1 )
		return(rmsg)
	# doge is special too!
	elif 'doge' == coin.lower():
		try:
			rmsg = ""
			rmsg = rmsg + get_pair(['DOGE'], ["BTC", "ETH", "EUR", "USD", "JPY", "DOGE"])
			rmsg = rmsg + "\n*1 DOGE is 1 DOGE is 1 DOGE!*\n\n"
		except:
			log.debug("exeption handling..")
			rmsg = rmsg + "Fehler beim laden der daten, bitte nochmal versuchen."
	elif 'all' == coin.lower():
		pass
	# defaultausgabe
	elif 'default' == coin.lower():
		# BTC ETH EUR USD XMR LTC
		rmsg=""
		rmsg = rmsg + get_pair(['BTC'], ['USD', 'EUR', 'JPY'])
		#rmsg = rmsg + get_pair(['ETH'], ['BTC', 'EUR', 'USD'])
		#rmsg = rmsg + get_pair(['LTC'], ['BTC', 'EUR', 'USD'])
		#rmsg = rmsg + get_pair(['XMR'], ['BTC', 'EUR', 'USD'])
		rmsg = rmsg + "(Quelle/Source [cryptocompare](http://www.cryptocompare.com/))"
		#bot.sendMessage(chat_id, rmsg, parse_mode="Markdown", disable_web_page_preview=1 )
		return(rmsg)
	elif 'test' == coin.lower():
		# BTC ETH EUR USD XMR LTC
		rmsg=""
		rmsg = rmsg + get_pair_test(['BTC'], ['USD', 'EUR', 'JPY'])
		rmsg = rmsg + get_pair_test(['DOGE'], ['USD', 'EUR', 'JPY', 'DOGE'])
		rmsg = rmsg + get_pair_test(['ETH'], ['BTC', 'EUR', 'USD'])
		rmsg = rmsg + get_pair_test(['LTC'], ['BTC', 'EUR', 'USD'])
		#rmsg = rmsg + get_pair(['XMR'], ['BTC', 'EUR', 'USD'])
		rmsg = rmsg + "(Quelle/Source [cryptocompare](http://www.cryptocompare.com/))"
		#bot.sendMessage(chat_id, rmsg, parse_mode="Markdown", disable_web_page_preview=1 )
		return(rmsg)
	# coin matrix.. lala
	elif 'matrix' == coin.lower():
		log.debug("Kurs ohne parameter aufgerufen!")
		# longer matrix
		#BTC DOGE ETH XMR BCH Wan iota zrx zebi waves
		#['BTC'],	['BTC', 'EUR', 'USD', 'ETH', 'DOGE', 'XMR',  'LTC'])
		#['DOGE'],	['BTC', 'EUR', 'USD', 'ETH', 'DOGE', 'XMR',  'LTC'])
		#['ETH'],	['BTC', 'EUR', 'USD', 'ETH', 'DOGE', 'XMR',  'LTC'])
		#['XMR'],	['BTC', 'EUR', 'USD', 'ETH', 'DOGE', 'XMR',  'LTC'])
		#['LTC'],	['BTC', 'EUR', 'USD', 'ETH', 'DOGE', 'XMR',  'LTC'])
		#```
		#BTC     BTC    DOGE   EUR    USD    ETH    XMR
		#BTC  1.0000 0.0000 0.0000 0.0000 0.0000 0.0000
		#DOGE 0.0000 1.0000 0.0000 0.0000 0.0000 0.0000
		#EUR  0.0000 0.0000 1.0000 0.0000 0.0000 0.0000
		#USD  0.0000 0.0000 0.0000 1.0000 0.0000 0.0000
		#ETH  0.0000 0.0000 0.0000 0.0000 1.0000 0.0000
		#XMR  0.0000 0.0000 0.0000 0.0000 0.0000 1.0000
		#LTC  0.0000 0.0000 0.0000 0.0000 0.0000 0.0000
		#```
		fsym = ['BTC', 'DOGE', 'ETH', 'EUR', 'USD']
		tsym = ['BTC', 'DOGE', 'ETH', 'EUR', 'USD']
		for index,fromsymbol in enumerate(fsym):
			if index == 0:
				rmsg = rmsg + "`" + fromsymbol.rjust(16) + "` "
			else:
				rmsg = rmsg + "`" + fromsymbol.rjust(11) + "` "
		rmsg = rmsg + "\n"
		for fromsymbol in fsym:
			log.debug(" symbol: " + fromsymbol)
			coindata = price.get_current_price(fromsymbol.upper(), tsym)
			rmsg = rmsg + "`" + fromsymbol.ljust(5,' ') + "`"
			log.debug("\n" + rmsg)
			for tosymbolindex,tosymbol in enumerate(tsym):
				log.debug(tosymbol)
				thisfloat = "%.4f" % float(coindata[fromsymbol][tosymbol])
				rmsg = rmsg + ' `'+ str(thisfloat).rjust(11,' ') + '`'
			rmsg = rmsg + str('\n')
		log.debug(" sending:\n" + rmsg)
		#bot.sendMessage(chat_id, rmsg, parse_mode="Markdown", disable_web_page_preview=1 )
		return(rmsg)

	# jeder andere parameter
	else:
		try:
			log.debug("kurs coin daten (" + coin + ") gefunden, auswertung")
			if coin.upper() in known_cryptos:
				coin = coin.upper()
				log.debug("kurs coin = " + coin + " gefunden.")
				rmsg = rmsg + get_pair([coin], ["DOGE", "ETH", "USD", "EUR", "BTC"])
				rmsg = rmsg + "\n(Quelle/Source [cryptocompare](http://www.cryptocompare.com/))"
			else:
				rmsg = "Das symbol `" + coin + "` konnte ich leider nicht finden."
		except:
			log.debug("exeption handling..")
			rmsg = rmsg + "Fehler beim laden der daten, bitte nochmal versuchen."
	log.debug("Reply:\n\n" + rmsg)
#	bot.sendMessage(chat_id, rmsg, parse_mode="Markdown", disable_web_page_preview=1 )
	return(rmsg)
################################################################################
def get_pair(fsym,tsym):
	log.debug("get_pair....")
	rmsg = ""
	for fromsymbol in fsym:
		log.debug("get_pair symbol " + str(fromsymbol))
		if fromsymbol.upper() in known_cryptos:
			coindata = price.get_current_price(fromsymbol.upper(), tsym)
			log.debug("coindata:" + str(coindata))
			for item,tosymbol in enumerate(tsym):
				log.debug("fromsymbol = " + fromsymbol + " tosymbol: " + tosymbol)
				if not fromsymbol.upper() == tosymbol.upper():
					#if tosymbol.upper() == 'EUR' or tosymbol.upper() == 'USD' or tosymbol.upper() == 'JPY' or tosymbol.upper() == 'GBP':
					#	if '{:0,.2f}'.format(coindata[fromsymbol][tosymbol.upper()]) == '0.00':
					#		thisfloat = '{:0,.8f}'.format(coindata[fromsymbol][tosymbol.upper()])
					#	else:
					#		thisfloat = '{:0,.2f}'.format(coindata[fromsymbol][tosymbol.upper()])
					#else:
					#	thisfloat = '{:0,.8f}'.format(coindata[fromsymbol][tosymbol.upper()])
					thisfloat = '{:0,.8f}'.format(coindata[fromsymbol][tosymbol.upper()])
					thisfloat = thisfloat.replace('.','#')
					thisfloat = thisfloat.replace(',','.')
					thisfloat = thisfloat.replace('#',',')
					while thisfloat.strip()[len(thisfloat)-1:len(thisfloat)] == '0':
						if thisfloat.strip()[len(thisfloat)-2:len(thisfloat)-1] == "." or thisfloat.strip()[len(thisfloat)-2:len(thisfloat)-1] == ",":
							break
						else:
							thisfloat = thisfloat.strip()[:len(thisfloat)-1]
					rmsg = rmsg + str('`') + str(fromsymbol.upper()).rjust(6) + str('`/`') + str(tsym[item]).ljust(6) + str('` *') + str(thisfloat) + str('*\n')
		else:
			rmsg = "Das symbol `" + coin + "` konnte ich leider nicht finden."
		return(rmsg)
################################################################################
################################################################################
log.debug("END!...")
