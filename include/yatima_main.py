#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
################################################################################
################################################################################
# first imports
import os, sys, inspect
################################################################################
################################################################################
scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))
if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
	scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
else:
	scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includepath	= scriptpath + '/include'
if os.path.isdir(includepath):
	sys.path.insert(0, includepath)
else:
	print("No include path found!")
	sys.exit(23)
################################################################################
################################################################################
import shutil, time, datetime, locale, json, logging, logging.handlers
import operator, argparse, psycopg2, asyncio
################################################################################
from pudb import set_trace 


# telepot! our primary resource! ;)
import telepot
import telepot.aio
import telepot.aio.helper
from telepot import message_identifier, glance, flance
from telepot.aio.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from telepot.aio.delegate import (
	include_callback_query_chat_id,
	per_message,
	per_chat_id,
	per_chat_id_in,
	per_application,
	call,
	create_open,
	pave_event_space)

#from messageincoming_helper import MessageIncoming
#from unreadstore_helper import UnreadStore
#from ownerhandler_helper import OwnerHandler
#from messagesaver_helper import MessageSaver

################################################################################
################################################################################
log = logging.getLogger(__name__)
log.debug("START!...")
################################################################################
################################################################################
################################################################################
class Yatima(telepot.aio.DelegatorBot):
	log.debug("Class Load")
	################################################################################
	def __init__(self,config,loop):
#		log.debug("telepot.aio.DelegatorBot = "+ str((telepot.aio.DelegatorBot)))
		log.debug("self=".rjust(config.spacing) +' '+ str(self))
		log.debug("config=".rjust(config.spacing) +' '+ str(config))
		log.debug("loop=".rjust(config.spacing) +' '+ str(loop))
		
		self.config = config

		# initialize (and save) database connection
		log.debug("Getting database stuff...")
		from db_helper import get_db_connection, get_db_owner_users
		self.dbcon = get_db_connection(self.config)

		self.owners = get_db_owner_users(self)

		# mkay, let's initialize out bot.
		log.debug("Initializing bot...")

		from yatima_helper import MsgHandler
#		from eventing_helper import Eventing
		################################################################################
		super(Yatima, self).__init__(self.config.telegram_token, [
			# Here is a delegate to specially handle owner commands.
#			pave_event_space()
#				(per_chat_id(types=['private']), create_open, MsgHandler, timeout=60),

			pave_event_space()
				(per_message(), create_open, MsgHandler, timeout=60),

			# here i want to start some cron like fubar...
#			pave_event_space()
#				(per_application(), create_open, EventHandler, timeout=60),

		])
		################################################################################
		# gather some data and
		self._get_bot_data(loop)
		# send some info to log... (and owner)
		self._send_start_info(loop)
		################################################################################
		# this shall be the initialisation of the cron like fubar above
#		self.router.routing_table['_alarm'] = self.on__alarm
#		self.delay = 5
#		self.scheduler.event_later(self.delay, ('_alarm', {'payload': self.delay, 'msg': 'lalala'}))

		#we're done! ;)
		log.debug("END...")

	################################################################################
	async def on_chat_message(self, msg):
		log.debug("...")
		try:
			delay = float(msg['text'])

			# 3. Schedule event
			#      The second argument is the event spec: a 2-tuple of (flavor, dict).
			# Put any custom data in the dict. Retrieve them in the event-handling function.
			self.scheduler.event_later(delay, ('_alarm', {'payload': delay}))
			await self.sender.sendMessage('Got it. Alarm is set at %.1f seconds from now.' % delay)
		except ValueError:
			await self.sender.sendMessage('Not a number. No alarm set.')
		log.debug("...")

	################################################################################
	def _get_bot_data(self,loop):
		# Lets get and debug-log some data..
		log.debug("Getting bot data...")
		self.bot_data = loop.run_until_complete(asyncio.gather(self.getMe()))[0]
		self.username	= self.bot_data['username']
		self.id			= self.bot_data['id']
		self.first_name	= self.bot_data['first_name']
		self.is_bot		= self.bot_data['is_bot']
		if self.config.debugmode:
			log.debug(     "our owner is:".rjust(self.config.spacing) +' '+ str(self.config.owner_id))
			log.debug(     "got bot data:".rjust(self.config.spacing) +' '+ str(self.bot_data))
			log.debug(        "Our ID is:".rjust(self.config.spacing) +' '+ str(self.id))
			log.debug(  "Our username is:".rjust(self.config.spacing) +' '+ str(self.username))
			log.debug("Our first name is:".rjust(self.config.spacing) +' '+ str(self.first_name))
		# test if we are actually a bot on telegram
		if self.is_bot:
			log.debug("We are a bot! Yay! ;)")
		else:
			log.critical("We are not a bot!!!".ljust(self.config.spacing))
			sys.exit(1)
		log.debug("Done...")
	################################################################################
	def _send_start_info(self,loop):
		log.debug("Sending info to owner...")
		# send a message to our owner

		msg = "I'am " + self.first_name+ " my username is " + self.username + " and my ID is " + str(self.id) + "\n\n"
		msg = msg + "I've been started and initialized at " + '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now()) + "\n\n"
		if len(self.owners) > 1:
			msg = msg + "My Owners have the IDs: "
			for entry in self.owners:
				if entry == self.owners[len(self.owners)-1]:
					msg = msg + str(entry) + ". "
				else:
					msg = msg + str(entry) + ", "
		else:
			msg = msg + "My owner has the ID: " + str(self.owners)
		msg = msg + "\n\n"

		msg = msg + "I'm fully functional and all my routines are working perfectly."
		msg = msg + "\n\n"
		msg = msg + "/start" + "\n"
		msg = msg + "/start@" + self.username + "\n"

		msg = msg + "/start fubarbaz" + "\n"
		msg = msg + "/start@funotbarexistingbazbotnamestring" + "\n"

		msg = msg + "/kurs" + "\n"
		msg = msg + "/kurs lala" + "\n"
		msg = msg + "/kurs@" + self.username + "\n"
		msg = msg + "/kurs@" + self.username + " lala\n"

		loop.run_until_complete(asyncio.gather(self.sendMessage(self.config.owner_id, msg)))
		log.debug("Done.")

	log.debug("Class load end...")
################################################################################
################################################################################
################################################################################
################################################################################
log.debug("END!...")
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
#
#	telepot.delegate
#		telepot.delegate.per_chat_id(types='all')[source]
#			Parameters:	types – all or a list of chat types (private, group, channel)
#			Returns:	a seeder function that returns the chat id only if the chat type is in types.
#
#		telepot.delegate.per_chat_id_in(s, types='all')[source]
#			Parameters:	
#				s - a list or set of chat id
#				types – all or a list of chat types (private, group, channel)
#		Returns:	
#		a seeder function that returns the chat id only if the chat id is in s and chat type is in types.
#		
#		telepot.delegate.per_chat_id_except(s, types='all')[source]
#		Parameters:	
#		s – a list or set of chat id
#		types – all or a list of chat types (private, group, channel)
#		Returns:	
#		a seeder function that returns the chat id only if the chat id is not in s and chat type is in types.
#		
#		telepot.delegate.per_from_id(flavors=['chat', 'inline_query', 'chosen_inline_result'])[source]
#		Parameters:	flavors – all or a list of flavors
#		Returns:	a seeder function that returns the from id only if the message flavor is in flavors.
#		telepot.delegate.per_from_id_in(s, flavors=['chat', 'inline_query', 'chosen_inline_result'])[source]
#		Parameters:	
#		s – a list or set of from id
#		flavors – all or a list of flavors
#		Returns:	
#		a seeder function that returns the from id only if the from id is in s and message flavor is in flavors.
#		
#		telepot.delegate.per_from_id_except(s, flavors=['chat', 'inline_query', 'chosen_inline_result'])[source]
#		Parameters:	
#		s – a list or set of from id
#		flavors – all or a list of flavors
#		Returns:	
#		a seeder function that returns the from id only if the from id is not in s and message flavor is in flavors.
#		
#		telepot.delegate.per_inline_from_id()[source]
#		Returns:	a seeder function that returns the from id only if the message flavor is inline_query or chosen_inline_result
#		telepot.delegate.per_inline_from_id_in(s)[source]
#		Parameters:	s – a list or set of from id
#		Returns:	a seeder function that returns the from id only if the message flavor is inline_query or chosen_inline_result and the from id is in s.
#		telepot.delegate.per_inline_from_id_except(s)[source]
#		Parameters:	s – a list or set of from id
#		Returns:	a seeder function that returns the from id only if the message flavor is inline_query or chosen_inline_result and the from id is not in s.
#		telepot.delegate.per_application()[source]
#		Returns:	a seeder function that always returns 1, ensuring at most one delegate is ever spawned for the entire application.
#		telepot.delegate.per_message(flavors='all')[source]
#		Parameters:	flavors – all or a list of flavors
#		Returns:	a seeder function that returns a non-hashable only if the message flavor is in flavors.
#		telepot.delegate.per_event_source_id(event_space)[source]
#		Returns:	a seeder function that returns an event’s source id only if that event’s source space equals to event_space.
#		telepot.delegate.per_callback_query_chat_id(types='all')[source]
#		Parameters:	types – all or a list of chat types (private, group, channel)
#		Returns:	a seeder function that returns a callback query’s originating chat id if the chat type is in types.
#		telepot.delegate.per_callback_query_origin(origins='all')[source]
#		Parameters:	origins – all or a list of origin types (chat, inline)
#		Returns:	a seeder function that returns a callback query’s origin identifier if that origin type is in origins. The origin identifier is guaranteed to be a tuple.
#		telepot.delegate.per_invoice_payload()[source]
#		Returns:	a seeder function that returns the invoice payload.
#		telepot.delegate.call(func, *args, **kwargs)[source]
#		Returns:	a delegator function that returns a tuple (func, (seed tuple,)+ args, kwargs). That is, seed tuple is inserted before supplied positional arguments. By default, a thread wrapping func and all those arguments is spawned.
#		telepot.delegate.create_run(cls, *args, **kwargs)[source]
#		Returns:	a delegator function that calls the cls constructor whose arguments being a seed tuple followed by supplied *args and **kwargs, then returns the object’s run method. By default, a thread wrapping that run method is spawned.
#		telepot.delegate.create_open(cls, *args, **kwargs)[source]
#		Returns:	a delegator function that calls the cls constructor whose arguments being a seed tuple followed by supplied *args and **kwargs, then returns a looping function that uses the object’s listener to wait for messages and invokes instance method open, on_message, and on_close accordingly. By default, a thread wrapping that looping function is spawned.
#		telepot.delegate.until(condition, fns)[source]
#		Try a list of seeder functions until a condition is met.
#		
#		Parameters:	
#		condition – a function that takes one argument - a seed - and returns True or False
#		fns – a list of seeder functions
#		Returns:	
#		a “composite” seeder function that calls each supplied function in turn, and returns the first seed where the condition is met. If the condition is never met, it returns None.
#		
#		telepot.delegate.chain(*fns)[source]
#		Returns:	a “composite” seeder function that calls each supplied function in turn, and returns the first seed that is not None.
#		telepot.delegate.pair(seeders, delegator_factory, *args, **kwargs)[source]
#		The basic pair producer.
#		
#		Returns:	a (seeder, delegator_factory(*args, **kwargs)) tuple.
#		Parameters:	seeders – If it is a seeder function or a list of one seeder function, it is returned as the final seeder. If it is a list of more than one seeder function, they are chained together before returned as the final seeder.
#		telepot.delegate.pave_event_space(fn=<function pair>)[source]
#		Returns:	a pair producer that ensures the seeder and delegator share the same event space.
#		telepot.delegate.include_callback_query_chat_id(fn=<function pair>, types='all')[source]
#		Returns:	a pair producer that enables static callback query capturing across seeder and delegator.
#		Parameters:	types – all or a list of chat types (private, group, channel)
#		telepot.delegate.intercept_callback_query_origin(fn=<function pair>, origins='all')[source]
#		Returns:	a pair producer that enables dynamic callback query origin mapping across seeder and delegator.
#		Parameters:	origins – all or a list of origin types (chat, inline). Origin mapping is only enabled for specified origin types.
#		
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################


################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
#class EventHandler(telepot.aio.helper.ChatHandler):
class EventHandler(telepot.aio.helper.Monitor):
	log.debug("Class Loading....")
	################################################################################
	def __init__(self, *args, **kwargs):
		log.debug("START!....")
		super(Eventing, self).__init__(*args, **kwargs)

		# 1. Customize the routing table:
		#      On seeing an event of flavor `_alarm`, call `self.on__alarm`.
		# To prevent flavors from colliding with those of Telegram messages,
		# events are given flavors prefixed with `_` by convention. Also by
		# convention is that the event-handling function is named `on_`
		# followed by flavor, leading to the double underscore.
		self.router.routing_table['_alarm'] = self.on__alarm
		log.debug("END!....")
	################################################################################
	async def on_chat_message(self, msg):
		log.debug("START!....")
		try:
			delay = float(msg['text'])

			# 3. Schedule event
			#      The second argument is the event spec: a 2-tuple of (flavor, dict).
			# Put any custom data in the dict. Retrieve them in the event-handling function.
			self.scheduler.event_later(delay, ('_alarm', {'payload': delay}))
			await self.sender.sendMessage('Got it. Alarm is set at %.1f seconds from now.' % delay)
		except ValueError:
			await self.sender.sendMessage('Not a number. No alarm set.')
		log.debug("END!....")
	################################################################################
	async def on__alarm(self, event):
		log.debug("START!...")
		print(event)  # see what the event object actually looks like
		await self.sender.sendMessage('Beep beep, time to wake up!')
		log.debug("END!...")
		return None
	log.debug("Class Loaded...")
	################################################################################
	async def _callback(**kwargs):
		log.debug("kwargs =".rjust(42) +' '+ str(kwargs))
		return

################################################################################
################################################################################
################################################################################
################################################################################
