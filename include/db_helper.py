#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
$ python3.6 include/db_helper.py

make my nice database work! ;)

(20180826T192514 UTC, hackbyte - daniel mitzlaff / me@hackbyte.de)

"""
################################################################################
# first imports
import os, sys, shutil, time, datetime, locale, asyncio, json
import logging, logging.handlers, inspect, operator, argparse
import select, psycopg2, psycopg2.extensions
log = logging.getLogger()
from datetime import time, date, datetime, timedelta
################################################################################
scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))
if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
    scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
else:
    scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includepath	= scriptpath + '/include'
if os.path.isdir(includepath):
    sys.path.insert(0, includepath)
else:
    print("No include path found!")
    sys.exit(23)
from pudb import set_trace 
log.debug("program '"+scriptname+"' start ")
################################################################################
# yeah..........................................................................
################################################################################
# Initiate database connection (try to be safe for async io use!)
# we get config data as parameter (bot.config from yatima).

def get_db_connection(config):
	log.debug("get_db_connection start...")
	# create connection object
	if config.testenv:
		log.debug(      "database_host:".rjust(config.spacing) +' '+ str(config.database_host))
		log.debug(      "database_name:".rjust(config.spacing) +' '+ str(config.database_name))
		log.debug(      "database_user:".rjust(config.spacing) +' '+ str(config.database_user))
		log.debug(  "database_password:".rjust(config.spacing) +' '+ str(config.database_password))
	dbcon = psycopg2.connect(
		host	=config.database_host,
		database=config.database_name,
		user	=config.database_user,
		password=config.database_password)
#		async=1
	# wait for the object to get ready
	log.debug("waiting for connection....")
	wait_for_db(dbcon)
	# and return it...

	if config.testenv:
		log_db_state(config,dbcon)
	log.debug("database connection ready!")
	log.debug("returning: " + str(dbcon))
	return(dbcon)

################################################################################
# debug log some status info..
def log_db_state(config,dbcon):
	log.debug(  "dbcon.server_version:".rjust(config.spacing) +' '+ str(dbcon.server_version))
	log.debug("dbcon.protocol_version:".rjust(config.spacing) +' '+ str(dbcon.protocol_version))
	log.debug(          "dbcon.status:".rjust(config.spacing) +' '+ str(dbcon.status))
	log.debug(           "dbcon.async:".rjust(config.spacing) +' '+ str(dbcon.async))
	log.debug(      "dbcon.autocommit:".rjust(config.spacing) +' '+ str(dbcon.autocommit))
	log.debug( "dbcon.isolation_level:".rjust(config.spacing) +' '+ str(dbcon.isolation_level))
	log.debug(  "dbcon.cursor_factory:".rjust(config.spacing) +' '+ str(dbcon.cursor_factory))
	log.debug(      "dbcon.deferrable:".rjust(config.spacing) +' '+ str(dbcon.deferrable))
	log.debug(        "dbcon.encoding:".rjust(config.spacing) +' '+ str(dbcon.encoding))
	log.debug(        "dbcon.readonly:".rjust(config.spacing) +' '+ str(dbcon.readonly))
	log.debug(          "dbcon.closed:".rjust(config.spacing) +' '+ str(dbcon.closed))
	return None
################################################################################
# wait for dbcon object to get ready
#
def wait_for_db(dbcon):
	log.debug("wait() for database connection...")
	# forever run
	while 1:
		# poll for connection state
		log.debug("polling state")
		state = dbcon.poll()
		# if state is POLL_OK
		if state == psycopg2.extensions.POLL_OK:
			log.debug("poll() returned POLL_OK")
			# stop our endless loop
			break
		elif state == psycopg2.extensions.POLL_WRITE:
			log.debug("poll() returned POLL_WRITE")
			select.select([], [dbcon.fileno()], [])
		elif state == psycopg2.extensions.POLL_READ:
			log.debug("poll() returned POLL_READ")
			select.select([dbcon.fileno()], [], [])
		else:
			log.error("poll() returned %s" % state)
			raise psycopg2.OperationalError("poll() returned %s" % state)

################################################################################
def get_db_owner_users(yatima):
	log.debug("START!!")
	dbcur = yatima.dbcon.cursor()

	# all users from db
	dbquery = "SELECT * FROM yatima_users WHERE owner='t';"

	# testing with only one user(!)
#	dbquery = "SELECT * FROM yatima_users WHERE id=1;"

	log.info("dbquery:".rjust(yatima.config.spacing) +' '+ dbquery)
	dbcur.execute(dbquery)

	log.info("got:".rjust(yatima.config.spacing) +' '+ str(dbcur.rowcount) + " rows.")

	if dbcur.rowcount > 0:
		owner_list = []
		for entry in dbcur.fetchall():
			if yatima.config.testenv:
				log.info("".rjust(yatima.config.spacing+1) + str(entry))
			log.info("adding UID " + str(entry[2]) + " to owner_list")
			owner_list.append(int(entry[2]))
	else:
		log.critical("Keine daten in der DB gefunden!")
		sys.exit(42)
	log.debug("returning: " + str(owner_list))
	log.debug("END!!")
	return(owner_list)

################################################################################
################################################################################
################################################################################
# alle tabellen loeschen...
def drop_all_tables(cbcon,dbcur):
	# alle tabellen laden und loeschen! ;)
	thisquery = "DROP TABLE "
	# tabellenliste besorgen
	tables=table_list()
	# wenn mehr als 0 eintraege (= len(tables)-1!)
	if len(tables)-1 > 0:
		# für alle einträge
		for index,table in enumerate(tables):
			# wenn index kleiner als len(tables)-1(!)
			if index < len(tables)-1:
				# tabelle hinzufügen mit ,
				thisquery = thisquery + table + ", "
			# wenn index gleich len(tables)-1(!)
			if index == len(tables)-1:
				# tabelle hinzufügen _ohne_ ,
				thisquery = thisquery + table
	# oder 0 eintraege (= len(tables)-1!)
	else:
		# tabelle hinzufügen
		thisquery = thisquery + tables
	# query mit einem ; abschliessen	
	thisquery = thisquery + ";"
	# query loggen
	log.info(lineno() + thisquery)
	# query ausführen
	dbcur.execute(thisquery)
	# änderungen an die db senden
	dbcon.commit()
	return
################################################################################
# alle user-tabellen laden
def table_list(dbcon):
	dbcur = dbcon.cursor()
	# query vorbereiten
	dbquery = "select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';"
	# query loggen
	log.debug("dbquery:\n" + dbquery)
	# query an die db schicken
	dbcur.execute(dbquery)
	# leere liste definieren
	tables = list()
	# für jeden eintrag von der tabelle
	for num,table in enumerate(dbcur.fetchall()):
		# zeile (datensatz) loggen
		log.debug("appending: " + table[0])
		# tabelle an liste anhängen...
		tables.append(table[0])
	# liste tables zurückgeben...
	return(tables)
################################################################################
#
# eine tabelle laden und ausgeben
#
def print_table(this_table):
	# versuche:
	try:
		# dbquery festlegen
		dbquery = "SELECT * FROM " + this_table+ ";"
		# dbquery loggen
		log.info(lineno() + "dbquery:\n " + dbquery)
		# dbquery ausführen mit dbexecute
		dbcur.execute(dbquery)
		# tabelle ausgeben
		printall(dbcur)
	except:
		log.error("print_table fehlgeschlagen!")
		sys.exit(42, msg)
################################################################################
#
# alle zeilen raushauen... ;)
#
def print_all_tables(dbcon):
	dbcur = dbcon.cursor()
	try:
		for entry in dbcur.fetchall():
			log.info(lineno() + str(entry))
	except:
		log.errror(lineno() + "printall fehlgeschlagen!!")
		sys.exit(42)
	return None
################################################################################
#
# superuser eruieren
def is_superuser(tgid):
	dbquery = "SELECT superuser FROM yatima_users where tgid = " + str(tgid) + ";"
	log.debug(lineno() + "dbquery:\n " + dbquery)
	dbcur.execute(dbquery)
	superuser=dbcur.fetchone()
	return(superuser)
#if is_superuser(290878813):
#	print("Yeah baby")
#else:
#	print("Fubar")


################################################################################
#
# owner eruieren
def is_owner(tgid):
	dbquery = "SELECT owner FROM yatima_users where tgid = " + str(tgid) + ";"
	log.debug(lineno() + "dbquery:\n " + dbquery)
	dbcur.execute(dbquery)
	superuser=dbcur.fetchone()
	return(superuser)
#if is_superuser(290878813):
#	print("Yeah baby")
#else:
#	print("Fubar")
################################################################################

################################################################################
def test_user_older_than():
	#
	#test for user older than X days (2)
	this_table = "butlernewgroupmembers"
	justnow  = datetime.now()
	justsoon = datetime.now() + timedelta(days=30)
	justold  = datetime.now() - timedelta(days=30)
	log.info(lineno() + "justnow  = " + str(justnow))
	log.info(lineno() + "justsoon = " + str(justsoon))
	log.info(lineno() + "justold  = " + str(justold))

	## selektiere alle einträge die juenger als 2 tage sind
	#dbquery = "SELECT * FROM " + this_table + " WHERE timestamp < " + str(nowtimestamp) + ";"
	#log.info(lineno() + dbquery)
	#dbcur.execute(dbquery)
	#
	##
	#for row in dbcur.fetchall():
	#  log.info(lineno() + str(row))
	#
	#
	#deltanow = datetime.now()

	#selectednow = int(datetime.now().strftime('%Y%m%d%H%M%S'))
	#log.info(lineno() + str(selectednow))
################################################################################
# convert bigint date (YYYYMMDDhhmmss) to datetime tuple
def bigint_to_datetime(bigint):
	thisdatetime = datetime(int(str(bigint).strip()[:4]),
		int(str(bigint).strip()[4:6]),
		int(str(bigint).strip()[6:8]),
		int(str(bigint).strip()[8:10]),
		int(str(bigint).strip()[10:12]),
		int(str(bigint).strip()[12:14]))
	return(thisdatetime)
################################################################################
# convert datetime tuple to bigint (YYYYMMDDhhmmss)
def datetime_to_bigint(mydatetime):
	return(mydatetime.strftime('%Y%m%d%H%M%S'))
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
log.debug("db_helper end...")
