#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

import json, os, sys, shutil, time, datetime, logging, logging.handlers, inspect, locale, operator
from pudb import set_trace 

log = logging.getLogger()

def a():
	return("\u00e4")
def A():
	return("\u00c4")
def u():
	return("\u00fC")
def U():
	return("\u00DC")
def o():
	return("\u00F6")
def O():
	return("\u00D6")
def sz():
	return( "\u00DF")
def SZ():
	return( "\u1E9E")

