#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

import json, os, sys, shutil, time, datetime, inspect, locale, operator
import logging, logging.handlers
log = logging.getLogger()
################################################################################
scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))
if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
	scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
else:
	scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includepath	= scriptpath + '/include'
if os.path.isdir(includepath):
	sys.path.insert(0, includepath)
else:
	print("No include-path found!")
	sys.exit(23)
from pudb import set_trace 

log.debug("START...")

# define Yatimaconfig class
class Yatimaconfig(object):
	"""configuration of yatima....
	"""
	log.debug("class load...")
	def __init__(self, config_file):

		################################################################################
		# hello, is there anybody out there?
		log.debug("Yatimaconfig.__init__ initializing")
		
		# here come the defaults, most of them are overwritten by loading
		# the config file later on below the defaults.
		# some of them are potentially  overwritten by cmdline params even later on..

		log.debug("Setting some config defaults...")

		# set config_file from args
		self.config_file = config_file

		# save yatima home from scriptpath (canon....)
		self.home = scriptpath

		# locale from environment
		self.locale = locale.getlocale()

		# defautlocale
		self.defaultlocale = locale.getdefaultlocale()

		#Get the radix character (decimal dot, decimal comma, etc.).
		self.RADIXCHAR = locale.RADIXCHAR

		#Get the separator character for thousands (groups of three digits).
		self.THOUSEP = locale.THOUSEP


		log.debug("Checking for configfile: " + self.config_file)
		if not os.path.isfile(self.config_file):
			msg = "No config file found. Remember changing the name of config-sample.json to config.json"
			log.critical(msg)
			print(msg)
			sys.exit(1)
		else:
			log.debug("Found!")

		try:
			log.debug("Trying to open config file.")
			with open(self.config_file, 'r') as f:
				configdata = json.load(f)
			log.debug("success loading json file!")
		except:
			log.critical("Error while loading config file!")
			sys.exit(1)

		################################################################################
		#
		# re-set data from old config if not overwritten by file
		#
#		for item in defaultconfig:
#			if not item in config:
#				config[item] = defaultconfig[item]
#				#debug(item + " copied from defaults!")


		log.debug("Importing config data...")
		# Write our config values from json dict into our own attributes...
		self.home = scriptpath

		# set full path to config file
		if 'config_file' in configdata:
			self.config_file = configdata['config_file']

		#self.config_file = configfile

		# set full path to log dir
		self.log_dir	= configdata['log_dir']

		# locale from environment
		self.locale = locale.getlocale()

		# defautlocale
		self.defaultlocale = locale.getdefaultlocale()

		#Get the radix character (decimal dot, decimal comma, etc.).
		self.RADIXCHAR = locale.RADIXCHAR

		#Get the separator character for thousands (groups of three digits).
		self.THOUSEP = locale.THOUSEP

		self.password = configdata['password']
		self.telegram_token = configdata['telegram_token']
		self.database = configdata['database']
		self.database_user = configdata['database_user']
		self.database_password = configdata['database_password']
		self.database_name = configdata['database_name']
		self.database_host = configdata['database_host']
		self.log_level = configdata['log_level']
		self.owner_id = configdata['owner_id']
		self.spacing = 35
#		self.spacing = 42



		# if we dont have a telegram token.... 
		log.debug("Looking for a telegram token")
		if self.telegram_token == "":
		# we're useless.. Complain and exit.
			msg = "No token defined. You have to set it in config.json." + \
				"See config-sample.json"
			log.critical(msg)
			print(msg)
			sys.exit(42)

		# if our password is empty, it's bad ... really bad
		log.debug("Looking for password...")
		if self.password == "":
			# complain and exit....
			msg = "WARNING: Empty password for registering to use the bot." + \
				" It could be dangerous, because anybody could use this bot" + \
				" and forward messages to the channels associated to it"
			log.critical(msg)
			print(msg)
			sys.exit(1)


		log.debug("Loaded configuration data:")
		for key in sorted(self.__dict__.keys()):
			log.debug(key.rjust(24) + ": " + str(getattr(self,key)))

		log.debug("Yatimaconfig initialized!")
		return
	log.debug("class loaded...")

log.debug(scriptname+" end " + '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now()))
