#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
################################################################################
################################################################################
# first imports
import os, sys, inspect
################################################################################
################################################################################
scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))
if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
	scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
else:
	scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includepath	= scriptpath + '/include'
if os.path.isdir(includepath):
	sys.path.insert(0, includepath)
else:
	print("No include path found!")
	sys.exit(23)
################################################################################
################################################################################
import shutil, time, datetime, locale, json, logging, logging.handlers
import operator, argparse, psycopg2, asyncio
################################################################################
from pudb import set_trace 


# telepot! our primary resource! ;)
import telepot
import telepot.aio
import telepot.aio.helper
from telepot import message_identifier, glance, flance
from telepot.aio.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from telepot.aio.delegate import (
	include_callback_query_chat_id,
	per_message,
	per_chat_id,
	per_chat_id_in,
	per_application,
	call,
	create_open,
	pave_event_space)

#from unreadstore_helper import UnreadStore
#from ownerhandler_helper import OwnerHandler
#from messagesaver_helper import MessageSaver


################################################################################
################################################################################
log = logging.getLogger(__name__)
log.debug("START!...")
################################################################################
################################################################################
class MsgHandler(telepot.aio.helper.ChatHandler):
	log.debug("Class Loading...")

#	keyboard = InlineKeyboardMarkup(inline_keyboard=[[
#		InlineKeyboardButton(text='Yes', callback_data='yes'),
#		InlineKeyboardButton(text='um ...', callback_data='no'),
#		]])

	def __init__(self, *args, **kwargs):
		# get bot.config.spacing to local
		self.spacing = args[0][0].config.spacing
		log.debug(     "self:".rjust(self.spacing) +' '+ str(self))

		log.debug(             "*args[0] is".rjust(self.spacing) +' '+ str(args[0]))
		log.debug(          "*args[0][0] is".rjust(self.spacing) +' '+ str(args[0][0]))
		log.debug(         "self.spacing is".rjust(self.spacing) +' '+ str(self.spacing))

		super(MsgHandler, self).__init__(*args, **kwargs)

#		# Retrieve from database
#		global propose_records
#		self.dbcur = self.bot.dbcon.cursor()
#		log.debug(               "db cursor".rjust(self.spacing) +' '+ str(self.dbcur))
#		self.dbquery = "SELECT * FROM propose_records;"
#		log.debug(               "db query".rjust(self.spacing) +' '+ str(self.dbquery))
#		self.dbcur.execute(self.dbquery)

#		if self.dbcur.rowcount > 0:
#		if self.id in propose_records:
#			log.debug(          "self.id " + str(self.id) + " found in propose records")
#			self._count, self._edit_msg_ident = propose_records[self.id]
#			log.debug(             "self.id:".rjust(self.spacing) +' '+ str(self.id))
#			log.debug("self._edit_msg_ident:".rjust(self.spacing) +' '+ str(self._edit_msg_ident))
#
#			self._editor = telepot.aio.helper.Editor(self.bot, self._edit_msg_ident) if self._edit_msg_ident else None
#
#			log.debug("self._editor:".rjust(self.spacing) +' '+ str(self._editor))
#
#		else:
#			self._count = 0
#			self._edit_msg_ident = None
#			self._editor = None

		log.debug("done")

	async def on_chat_message(self, msg):
#		self.spacing = args[0][0].config.spacing
		log.debug(     "self:".rjust(self.spacing) +' '+ str(self))
		log.debug(      "msg:".rjust(self.spacing) +' '+ str(msg))
		content_type, chat_type, chat_id, chat_timestamp, chat_msgid = glance(msg,long=True)
		if self.bot.config.testenv:
			log.debug("content_type:".rjust(self.spacing) +' '+ str(content_type))
			log.debug("chat_type:".rjust(self.spacing) +' '+ str(chat_type))
			log.debug("chat_id:".rjust(self.spacing) +' '+ str(chat_id))
			log.debug("chat_timestamp:".rjust(self.spacing) +' '+ str(chat_timestamp))
			log.debug("chat_msgid:".rjust(self.spacing) +' '+ str(chat_msgid))

		self.rmsg = ""

		text=""
		if 'text' in msg:
			text = text + msg['text']
		if 'caption' in msg:
			# embrace it ;)
			text = text + msg['caption']

		if '/' == text[:1]:
			log.debug("Kommandozeichen '/' erkannt!")

			command = text[1:].lower()
#			log.debug("Kommando '" + command + "' gefunden")

			if '@' in command:
#				self.rmsg="@ in kommando entdeckt!\n\n"
				cmdvar, cmdargs = command.split('@')

				if str(self.bot.username).lower() in cmdargs[:len(self.bot.username)].lower():
					log.debug("command == " + command + " is for " + str(self.bot.username).lower() + " MATCH!")
#					cmdargs = cmdargs[len(self.bot.username):]
					log.debug("cmdvar      = " + str(cmdvar))
					log.debug("cmdargs     = " + str(cmdargs))
					log.debug("cmdargs len = " + str(len(cmdargs)))
					cmdargs = cmdargs[len(self.bot.username):]
					log.debug("cmdargs     = " + str(cmdargs))
					log.debug("cmdargs len = " + str(len(cmdargs)))
#					if len(rest) > 0:
					command = cmdvar + cmdargs
					log.debug("command is now: '" + command + "'")
					log.debug("command len is now: '" + str(len(command)) + "'")
				else:
					log.debug("command '" + command + "' is not for us, ignoring!")
					return None

#			log.debug("command == '" + str(command) + "'")


			if ' ' in command:
				log.debug("Leerzeichen in kommando")
				##FIXME## mehrere leerzeichen auswerten koennen
				##string.split(s[, sep[, maxsplit]])
				parselist = command.split(' ',1)
				command = parselist[0]
				cmdargs = parselist[1]

				log.debug("command = '" + command + "'")
				log.debug("cmdargs = '" + cmdargs + "'")
			else:
				pass
			
			log.debug("command      = '" + str(command) + "'") 
			log.debug("len(command) = '" + str(len(command)) + "'")
			log.debug("len('start') = '" + str(len('start')) + "'")
#			if 'start' == command[len('start')+1:]:
#			if 'start' in command:
			if command.lower() == 'start':
				log.debug("command = '" + command + "'\n")
				log.debug(    "len = '" + str(len(command)))
				self.rmsg = "Hallo, ich bin yatima, wurden wir uns schon vorgestellt?\n"
				self.rmsg = "\n"
				self.rmsg = "Ich glaube nicht, noch erkenne ich dich leider nicht.\n"
				log.debug("sending: " + self.rmsg)
				#await self.sender.sendMessage(self.rmsg)
				await self.bot.sendMessage(chat_id,self.rmsg)
				return None

#			elif 'kurs' == command[len('kurs'):]:
			elif command.lower() == 'kurs':
#				# wenn kurs mit irgendwelchen weiteren zeichen aufgerufen wurde
#				if len(txt.strip()[6:]) > 0:
#					coin = str(txt.strip()[6:])
				log.debug("elif 'kurs' == msg['text'].strip()[:5].lower() (=" + str(msg['text'].strip()[:5].lower()) + ")")
				from cryptocompy_helper import kurs_cmd
				try:
					self.rmsg = kurs_cmd(self.bot,msg)
				except:
					self.rmsg = "Es gab leider einen fehler beim laden der daten, bitte erneut versuchen."
	
				self.rmsg = kurs_cmd(self.bot,msg)
#				await self.sender.sendMessage(self.rmsg, parse_mode="Markdown", disable_web_page_preview=1)
				await self.bot.sendMessage(chat_id, self.rmsg, parse_mode="Markdown", disable_web_page_preview=1)
				return None



#		self.rmsg = "I'm afraid i can't do that dave."
#		await self.sender.sendMessage(self.rmsg)
#		log.debug("error, sending: " + self.rmsg)
		log.debug("MsgHandler done...")

	async def on__idle(self, event):
		log.debug(     "self:".rjust(self.spacing) +' '+ str(self))
		log.debug(    "event:".rjust(self.spacing) +' '+ str(event))
		log.debug("closing..")
		self.close()
		log.debug("done...")



#	def on_close(self, ex):
	def on_close(self, ex ):
		log.debug(     "self:".rjust(self.spacing) +' '+ str(self))
		log.debug(       "ex:".rjust(self.spacing) +' '+ str(ex))
#		global propose_records
#		propose_records[self.id] = (self._count, self._edit_msg_ident)
#		log.debug("propose_records[self.id] = (self._count, self._edit_msg_ident)")
#		log.debug(str(propose_records))
#		log.debug(str(self.id))
#		log.debug(str(self._count))
#		log.debug(str(self._edit_msg_ident))
		log.debug("done...")

	log.debug("Class Loaded...")
################################################################################
################################################################################
################################################################################
################################################################################
log.debug("END!...")
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################

#	telepot.delegate
#		telepot.delegate.per_chat_id(types='all')[source]
#			Parameters:	types – all or a list of chat types (private, group, channel)
#			Returns:	a seeder function that returns the chat id only if the chat type is in types.

#		telepot.delegate.per_chat_id_in(s, types='all')[source]
#			Parameters:	
#				s - a list or set of chat id
#				types – all or a list of chat types (private, group, channel)
#		Returns:	
#		a seeder function that returns the chat id only if the chat id is in s and chat type is in types.
#		
#		telepot.delegate.per_chat_id_except(s, types='all')[source]
#		Parameters:	
#		s – a list or set of chat id
#		types – all or a list of chat types (private, group, channel)
#		Returns:	
#		a seeder function that returns the chat id only if the chat id is not in s and chat type is in types.
#		
#		telepot.delegate.per_from_id(flavors=['chat', 'inline_query', 'chosen_inline_result'])[source]
#		Parameters:	flavors – all or a list of flavors
#		Returns:	a seeder function that returns the from id only if the message flavor is in flavors.
#		telepot.delegate.per_from_id_in(s, flavors=['chat', 'inline_query', 'chosen_inline_result'])[source]
#		Parameters:	
#		s – a list or set of from id
#		flavors – all or a list of flavors
#		Returns:	
#		a seeder function that returns the from id only if the from id is in s and message flavor is in flavors.
#		
#		telepot.delegate.per_from_id_except(s, flavors=['chat', 'inline_query', 'chosen_inline_result'])[source]
#		Parameters:	
#		s – a list or set of from id
#		flavors – all or a list of flavors
#		Returns:	
#		a seeder function that returns the from id only if the from id is not in s and message flavor is in flavors.
#		
#		telepot.delegate.per_inline_from_id()[source]
#		Returns:	a seeder function that returns the from id only if the message flavor is inline_query or chosen_inline_result
#		telepot.delegate.per_inline_from_id_in(s)[source]
#		Parameters:	s – a list or set of from id
#		Returns:	a seeder function that returns the from id only if the message flavor is inline_query or chosen_inline_result and the from id is in s.
#		telepot.delegate.per_inline_from_id_except(s)[source]
#		Parameters:	s – a list or set of from id
#		Returns:	a seeder function that returns the from id only if the message flavor is inline_query or chosen_inline_result and the from id is not in s.
#		telepot.delegate.per_application()[source]
#		Returns:	a seeder function that always returns 1, ensuring at most one delegate is ever spawned for the entire application.
#		telepot.delegate.per_message(flavors='all')[source]
#		Parameters:	flavors – all or a list of flavors
#		Returns:	a seeder function that returns a non-hashable only if the message flavor is in flavors.
#		telepot.delegate.per_event_source_id(event_space)[source]
#		Returns:	a seeder function that returns an event’s source id only if that event’s source space equals to event_space.
#		telepot.delegate.per_callback_query_chat_id(types='all')[source]
#		Parameters:	types – all or a list of chat types (private, group, channel)
#		Returns:	a seeder function that returns a callback query’s originating chat id if the chat type is in types.
#		telepot.delegate.per_callback_query_origin(origins='all')[source]
#		Parameters:	origins – all or a list of origin types (chat, inline)
#		Returns:	a seeder function that returns a callback query’s origin identifier if that origin type is in origins. The origin identifier is guaranteed to be a tuple.
#		telepot.delegate.per_invoice_payload()[source]
#		Returns:	a seeder function that returns the invoice payload.
#		telepot.delegate.call(func, *args, **kwargs)[source]
#		Returns:	a delegator function that returns a tuple (func, (seed tuple,)+ args, kwargs). That is, seed tuple is inserted before supplied positional arguments. By default, a thread wrapping func and all those arguments is spawned.
#		telepot.delegate.create_run(cls, *args, **kwargs)[source]
#		Returns:	a delegator function that calls the cls constructor whose arguments being a seed tuple followed by supplied *args and **kwargs, then returns the object’s run method. By default, a thread wrapping that run method is spawned.
#		telepot.delegate.create_open(cls, *args, **kwargs)[source]
#		Returns:	a delegator function that calls the cls constructor whose arguments being a seed tuple followed by supplied *args and **kwargs, then returns a looping function that uses the object’s listener to wait for messages and invokes instance method open, on_message, and on_close accordingly. By default, a thread wrapping that looping function is spawned.
#		telepot.delegate.until(condition, fns)[source]
#		Try a list of seeder functions until a condition is met.
#		
#		Parameters:	
#		condition – a function that takes one argument - a seed - and returns True or False
#		fns – a list of seeder functions
#		Returns:	
#		a “composite” seeder function that calls each supplied function in turn, and returns the first seed where the condition is met. If the condition is never met, it returns None.
#		
#		telepot.delegate.chain(*fns)[source]
#		Returns:	a “composite” seeder function that calls each supplied function in turn, and returns the first seed that is not None.
#		telepot.delegate.pair(seeders, delegator_factory, *args, **kwargs)[source]
#		The basic pair producer.
#		
#		Returns:	a (seeder, delegator_factory(*args, **kwargs)) tuple.
#		Parameters:	seeders – If it is a seeder function or a list of one seeder function, it is returned as the final seeder. If it is a list of more than one seeder function, they are chained together before returned as the final seeder.
#		telepot.delegate.pave_event_space(fn=<function pair>)[source]
#		Returns:	a pair producer that ensures the seeder and delegator share the same event space.
#		telepot.delegate.include_callback_query_chat_id(fn=<function pair>, types='all')[source]
#		Returns:	a pair producer that enables static callback query capturing across seeder and delegator.
#		Parameters:	types – all or a list of chat types (private, group, channel)
#		telepot.delegate.intercept_callback_query_origin(fn=<function pair>, origins='all')[source]
#		Returns:	a pair producer that enables dynamic callback query origin mapping across seeder and delegator.
#		Parameters:	origins – all or a list of origin types (chat, inline). Origin mapping is only enabled for specified origin types.
#		
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################





#		elif '/kurs' == msg['text'].strip()[:5].lower():
#			log.debug("elif '/kurs' == msg['text'].strip()[:5].lower() (=" + str(msg['text'].strip()[:5].lower()) + ")")
#			from cryptocompy_helper import kurs_cmd
#			try:
#				self.rmsg = kurs_cmd(self.bot,msg)
#			except:
#				self.rmsg = "Es gab leider einen fehler beim laden der daten, bitte erneut versuchen."
#
#			self.rmsg = kurs_cmd(self.bot,msg)
#			await self.sender.sendMessage(self.rmsg, parse_mode="Markdown", disable_web_page_preview=1)
#			return None
#		else:
#			self.rmsg = "Ach wir kennen uns doch schon......"
#			await self.sender.sendMessage(self.rmsg)
#			return None
