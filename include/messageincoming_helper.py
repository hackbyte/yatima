#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
################################################################################
################################################################################
# first imports
import os, sys, shutil, time, datetime, locale, asyncio, json
import logging, logging.handlers, inspect, operator, argparse
import psycopg2
################################################################################
################################################################################
scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))
if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
	scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
else:
	scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includepath	= scriptpath + '/include'
if os.path.isdir(includepath):
	sys.path.insert(0, includepath)
else:
	print("No include path found!")
	sys.exit(23)
################################################################################
################################################################################
from pudb import set_trace 
import telepot
from telepot.aio.loop import MessageLoop
from telepot.aio.delegate import (
	per_message,
	per_chat_id,
	per_chat_id_in,
	per_application,
	call,
	create_open,
	pave_event_space)
from unreadstore_helper import UnreadStore
from ownerhandler_helper import OwnerHandler
from messagesaver_helper import MessageSaver
################################################################################
################################################################################
log = logging.getLogger(__name__)
log.debug("START!...")
################################################################################
class MessageIncoming(telepot.aio.helper.Monitor):
	log.debug("Class Load...")
	def __init__(self, msg):

		log.debug("START!")

		# The `capture` criteria means to capture all messages.
		super(MessageIncoming, self).__init__(seed_tuple, capture=[[lambda msg: not telepot.is_event(msg)]], **kwargs)

	# Store every message, except those whose sender is in the exclude list, or non-text messages.
	def on_chat_message(self, msg):
		log.debug("args msg".rjust(self.spacing) +" "+str(msg))

#		'message_id': 655,
		if 'message_id' in msg:
			log.debug("msg[message_id]".rjust(self.spacing) +" "+ str(msg['message_id']))
#		'from': {
		if 'from' in msg:
#			'id': 290878813,
			if 'id' in msg['from']:
				log.debug("msg[from][id]".rjust(self.spacing) +" "+ str(msg['from']['id']))
#			'is_bot': False,
			if 'is_bot' in msg['from']:
				if msg['from']['is_bot']:
					log.debug("msg[from][is_bot]".rjust(self.spacing) +" "+ str(msg['from']['is_bot']) + " it's a bot!")
			else:
					log.debug("msg[from][is_bot]".rjust(self.spacing) +" "+ str(msg['from']['is_bot']) + " it's a user!")
#			'first_name': 'hackbyte',
			if 'first_name' in msg['from']:
				log.debug("msg[from][first_name]".rjust(self.spacing) +" "+ str(msg['from']['first_name']))
			if 'last_name' in msg['from']:
				log.debug("msg[from][last_name]".rjust(self.spacing) +" "+ str(msg['from']['last_name']))
#			'username': 'hackbyte',
			if 'username' in msg['from']:
				log.debug("msg[from][username]".rjust(self.spacing) +" "+ str(msg['from']['username']))
#			'language_code': 'en-US'
			if 'language_code' in msg['from']:
				log.debug("msg[from][language_code]".rjust(self.spacing) +" "+ str(msg['from']['language_code']))
#			},
#		'chat': {
		if 'chat'in msg:
#			'id': 290878813,
			if 'id' in msg['chat']:
				log.debug("msg[chat][id]".rjust(self.spacing) +" "+ str(msg['chat']['id']))	

#			'first_name': 'hackbyte',
			if 'first_name' in msg['chat']:
				log.debug("msg[chat][first_name]".rjust(self.spacing) +" "+ str(msg['chat']['first_name']))
			if 'last_name' in msg['chat']:
				log.debug("msg[chat][last_name]".rjust(self.spacing) +" "+ str(msg['chat']['last_name']))
#			'username': 'hackbyte',
			if 'username' in msg['chat']:
				log.debug("msg[chat][username]".rjust(self.spacing) +" "+ str(msg['chat']['username']))
#			'type': 'private'},
			if 'type' in msg['chat']:
				log.debug("msg[chat][type]".rjust(self.spacing) +" "+ str(msg['chat']['type']))
#		'date': 1535552954,
		if 'date' in msg:
			log.debug("msg[date]".rjust(self.spacing) +" "+ str(msg['date']))
#		'text': 'a'

		content_type, chat_type, chat_id = telepot.glance(msg)
		#log.debug("content_type".rjust(self.spacing) +" "+str(content_type))
		#log.debug("chat_type".rjust(self.spacing) +" "+str(chat_type))
		#log.debug("chat_id".rjust(self.spacing) +" "+str(chat_id))

		if chat_id in self._exclude:
			#print('Chat id %d is excluded.' % chat_id)
			log.debug('Chat id %d is excluded.' % chat_id)
			return
		else:
			log.debug('Chat id %d is NOT excluded.' % chat_id)

		if content_type != 'text':
			#print('Content type %s is ignored.' % content_type)
			log.debug('Content type %s is ignored.' % content_type)
			return
		else:
			log.debug('Content type %s is accepted.' % content_type)

		#print('Storing message: %s' % str(msg))
		log.debug('Storing message: %s' % str(msg))
		self._store.put(msg)

	log.debug("Class Loaded...")
################################################################################
