#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
################################################################################
# first imports
import os, sys, shutil, time, datetime, locale, asyncio, json
import logging, logging.handlers, inspect, operator, argparse
import psycopg2

################################################################################
# save script data
scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
scriptname	= os.path.basename(inspect.getfile(inspect.currentframe()))
if 'include' == os.path.basename(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))):
	scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
else:
	scriptpath	= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includepath	= scriptpath + '/include'
if os.path.isdir(includepath):
	sys.path.insert(0, includepath)
else:
	print("No include path found!")
	sys.exit(23)
from pudb import set_trace 
log = logging.getLogger()

log.debug("START!...")

def show_func(caller):
	log.debug("Gettin verbose log data fubar....")

	log.debug("caller = " + str(caller))

	if inspect.ismodule(caller):
		log.debug("caller is a module")
		#Return true if the object is a module.

	if inspect.isclass(caller):
		log.debug("caller is a class")
		#Return true if the object is a class, whether built-in or created in Python code.

	if inspect.ismethod(caller):
		log.debug("caller is a method")
		#Return true if the object is a bound or unbound method written in Python.

	if inspect.isfunction(caller):
		log.debug("caller is a function")
		#Return true if the object is a Python function, which includes functions created by a lambda expression.

	if inspect.isgeneratorfunction(caller):
		log.debug("Return true if the object is a Python generator function.")

	if inspect.isgenerator(caller):
		log.debug("Return true if the caller is a generator.")

	if inspect.istraceback(caller):
		log.debug("Return true if the caller is a traceback.")

	if inspect.isframe(caller):
		log.debug("Return true if the caller is a frame.")

	if inspect.iscode(caller):
		log.debug("Return true if the caller is a code.")

	if inspect.isbuiltin(caller):
		log.debug("Return true if the caller is a built-in function or a bound built-in method.")

	if inspect.isroutine(caller):
		log.debug("Return true if the caller is a user-defined or built-in function or method.")

	if inspect.isabstract(caller):
		log.debug("Return true if the caller is an abstract base class.")

	if inspect.ismethoddescriptor(caller):
		log.debug("Return true if the caller is a method descriptor, but not if ismethod(), isclass(), isfunction() or isbuiltin() are true.")

	if inspect.isdatadescriptor(caller):
		log.debug("Return true if the caller is a data descriptor.")

	if inspect.isgetsetdescriptor(caller):
		log.debug("Return true if the caller is a getset descriptor.")

	if inspect.ismemberdescriptor(caller):
		log.debug("Return true if the caller is a member descriptor.")
		log.debug("CPython implementation detail: Member descriptors are attributes defined in extension modules via PyMemberDef structures. For Python implementations without such types, this method will always return False.")


	log.debug("----------")
	log.debug("----------")
	log.debug("----------")




	sys.exit()
#	caller_name = inspect.getmembers(caller)[0]

#	set_trace()
#	caller_members = inspect.getmembers(caller)
#	for entry in inspect.getmembers(caller):
#		log.debug(" Entry: " + str(entry))

	#<yatima_main.Yatima object at 0x7fcc15aa65f8>
	# ('Scheduler', <class 'telepot.aio.Bot.Scheduler'>)
	# ('__class__', <class 'yatima_main.Yatima'>)
	# ('__delattr__', <method-wrapper '__delattr__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__dict__', {'config': <config_helper.Yatimaconfig object at 0x7fcc11bcc588>, '_seen': set(), '_store': <unreadstore_helper.UnreadStore object at 0x7fcc15f5b0f0>})
	# ('__dir__', <built-in method __dir__ of Yatima object at 0x7fcc15aa65f8>)
	# ('__doc__', None)
	# ('__eq__', <method-wrapper '__eq__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__format__', <built-in method __format__ of Yatima object at 0x7fcc15aa65f8>)
	# ('__ge__', <method-wrapper '__ge__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__getattribute__', <method-wrapper '__getattribute__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__gt__', <method-wrapper '__gt__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__hash__', <method-wrapper '__hash__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__init__', <bound method Yatima.__init__ of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('__init_subclass__', <built-in method __init_subclass__ of type object at 0x16e7108>)
	# ('__le__', <method-wrapper '__le__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__lt__', <method-wrapper '__lt__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__module__', 'yatima_main')
	# ('__ne__', <method-wrapper '__ne__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__new__', <built-in method __new__ of type object at 0x7fcc174afe60>)
	# ('__reduce__', <built-in method __reduce__ of Yatima object at 0x7fcc15aa65f8>)
	# ('__reduce_ex__', <built-in method __reduce_ex__ of Yatima object at 0x7fcc15aa65f8>)
	# ('__repr__', <method-wrapper '__repr__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__setattr__', <method-wrapper '__setattr__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__sizeof__', <built-in method __sizeof__ of Yatima object at 0x7fcc15aa65f8>)
	# ('__str__', <method-wrapper '__str__' of Yatima object at 0x7fcc15aa65f8>)
	# ('__subclasshook__', <built-in method __subclasshook__ of type object at 0x16e7108>)
	# ('__weakref__', None)
	# ('_api_request', <bound method Bot._api_request of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('_api_request_with_file', <bound method Bot._api_request_with_file of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('_seen', set())
	# ('_send_welcome', <bound method Yatima._send_welcome of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('_store', <unreadstore_helper.UnreadStore object at 0x7fcc15f5b0f0>)
	# ('_test_is_newcomer', <bound method Yatima._test_is_newcomer of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('addStickerToSet', <bound method Bot.addStickerToSet of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('answerCallbackQuery', <bound method Bot.answerCallbackQuery of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('answerInlineQuery', <bound method Bot.answerInlineQuery of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('answerPreCheckoutQuery', <bound method Bot.answerPreCheckoutQuery of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('answerShippingQuery', <bound method Bot.answerShippingQuery of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('config', <config_helper.Yatimaconfig object at 0x7fcc11bcc588>)
	# ('createNewStickerSet', <bound method Bot.createNewStickerSet of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('create_listener', <bound method SpeakerBot.create_listener of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('deleteChatPhoto', <bound method Bot.deleteChatPhoto of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('deleteChatStickerSet', <bound method Bot.deleteChatStickerSet of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('deleteMessage', <bound method Bot.deleteMessage of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('deleteStickerFromSet', <bound method Bot.deleteStickerFromSet of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('deleteWebhook', <bound method Bot.deleteWebhook of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('download_file', <bound method Bot.download_file of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('editMessageCaption', <bound method Bot.editMessageCaption of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('editMessageLiveLocation', <bound method Bot.editMessageLiveLocation of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('editMessageReplyMarkup', <bound method Bot.editMessageReplyMarkup of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('editMessageText', <bound method Bot.editMessageText of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('exportChatInviteLink', <bound method Bot.exportChatInviteLink of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('forwardMessage', <bound method Bot.forwardMessage of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getChat', <bound method Bot.getChat of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getChatAdministrators', <bound method Bot.getChatAdministrators of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getChatMember', <bound method Bot.getChatMember of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getChatMembersCount', <bound method Bot.getChatMembersCount of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getFile', <bound method Bot.getFile of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getGameHighScores', <bound method Bot.getGameHighScores of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getMe', <bound method Bot.getMe of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getStickerSet', <bound method Bot.getStickerSet of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getUpdates', <bound method Bot.getUpdates of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getUserProfilePhotos', <bound method Bot.getUserProfilePhotos of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('getWebhookInfo', <bound method Bot.getWebhookInfo of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('handle', <bound method DelegatorBot.handle of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('kickChatMember', <bound method Bot.kickChatMember of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('leaveChat', <bound method Bot.leaveChat of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('message_loop', <bound method Bot.message_loop of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('on__alarm', <bound method Yatima.on__alarm of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('pinChatMessage', <bound method Bot.pinChatMessage of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('promoteChatMember', <bound method Bot.promoteChatMember of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('restrictChatMember', <bound method Bot.restrictChatMember of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendAudio', <bound method Bot.sendAudio of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendChatAction', <bound method Bot.sendChatAction of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendContact', <bound method Bot.sendContact of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendDocument', <bound method Bot.sendDocument of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendGame', <bound method Bot.sendGame of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendInvoice', <bound method Bot.sendInvoice of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendLocation', <bound method Bot.sendLocation of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendMediaGroup', <bound method Bot.sendMediaGroup of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendMessage', <bound method Bot.sendMessage of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendPhoto', <bound method Bot.sendPhoto of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendSticker', <bound method Bot.sendSticker of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendVenue', <bound method Bot.sendVenue of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendVideo', <bound method Bot.sendVideo of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendVideoNote', <bound method Bot.sendVideoNote of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('sendVoice', <bound method Bot.sendVoice of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('setChatDescription', <bound method Bot.setChatDescription of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('setChatPhoto', <bound method Bot.setChatPhoto of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('setChatStickerSet', <bound method Bot.setChatStickerSet of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('setChatTitle', <bound method Bot.setChatTitle of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('setGameScore', <bound method Bot.setGameScore of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('setStickerPositionInSet', <bound method Bot.setStickerPositionInSet of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('setWebhook', <bound method Bot.setWebhook of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('stopMessageLiveLocation', <bound method Bot.stopMessageLiveLocation of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('unbanChatMember', <bound method Bot.unbanChatMember of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('unpinChatMessage', <bound method Bot.unpinChatMessage of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)
	# ('uploadStickerFile', <bound method Bot.uploadStickerFile of <yatima_main.Yatima object at 0x7fcc15aa65f8>>)


	log.debug("----------")
	return None


log.debug("END!...")

